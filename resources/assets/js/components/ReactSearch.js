import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import styled from 'styled-components';

const XButton = styled.button`
    cursor: pointer;
    position: absolute;
    outline: none;
    top: 5px;
    right: 5px;
    background: none;
    border: none;
    color: red;
    font-family: 'Montserrat', sans-serif;
    font-size: 18px;   
`;
const List = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
    position: absolute;
    top: 100%;
    left: 0;
    right: 0;
    background-color: white;
    border-radius: 4px;
    overflow: hidden;
    border: 3px solid rgba(218, 218, 218, 0.3);
`;
const ListItem = styled.li`
    width: 100%;
    height auto;
    
    &:hover {
        background-color: #05c1b6;
    }
    &:not(:last-child) {
        border-bottom: 1px solid rgba(218, 218, 218, 0.3);
    }
`;
const ToUser = styled.a`
    display: block;
    width: 100%;
    text-align: center;
    color: black;
    cursor: pointer;
    text-decoration: none;
    padding: 15px 0;
        
    &:hover, &.active {
        color: white;
        text-decoration: underline;
    }    
`;
const Empty = styled.div`
    display: block;
    width: 100%;
    text-align: center;
    color: black;
    text-decoration: none;
    padding: 15px 0; 
`;


export default class Example extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expression: '',
            results: null
        };
        this.onInputChangeHandler = this.onInputChangeHandler.bind(this);
    };

    onInputChangeHandler(e) {
        const newExpression = e.target.value;

        newExpression === ''
            ? this.setState({expression: '', results: null})
            : this.setState({expression: newExpression});

        if (newExpression && newExpression.length > 2) {
            setTimeout(() => {
                this.getSearchResults();
            }, 400);
        }
    };

    getSearchResults() {
        axios.get(`/api/search/${this.state.expression}`)
            .then(response => this.setState({results: response.data}))
            .catch(error => console.log(error.response.message));
    }

    render() {
        const list = this.state.results;

        let listContent = null,
            pathToCard = '/solo';

        if (window.location.pathname.indexOf('admin') > 0) {
            pathToCard = '/admin/card'
        }

        if (list) {
             if (list[0]) {
                 listContent =  <List>
                    {
                        list.map(item => <ListItem key={item.id}>
                                <ToUser href={`${pathToCard}/${item.id}`}>
                                    {item.name}
                                </ToUser>
                            </ListItem>
                        )}
                </List>;
             }
             else if (!list[0] && this.state.expression !== '')
             {
                 listContent = <List>
                     <ListItem>
                         <Empty onClick={() => this.setState({expression:'', results: null})}>
                            No results find.
                         </Empty>
                     </ListItem>
                 </List>;
             } else {
                 listContent = null;
             }
        }

        return (
            <div>
                <input
                    placeholder="Search..."
                    type="text"
                    onChange={e => this.onInputChangeHandler(e)}
                    value={this.state.expression}/>
                {
                    list
                        ? <XButton type="button" onClick={() => this.setState({expression:'', results: null})}>x</XButton>
                        : null
                }
                {listContent}
            </div>
        );
    }
}

if (document.getElementById('react-navbar-search')) {
    ReactDOM.render(<Example />, document.getElementById('react-navbar-search'));
}
