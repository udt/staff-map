<div class="copyable">
    <button type="button" class="copy-button">Copy</button>
    <div class="copy-content">
        {{ $content }}
    </div>
</div>