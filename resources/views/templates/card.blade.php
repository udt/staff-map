<div class="card-template">
    <div class="ellipse"></div>
    <div class="ellipse ellipse-2"></div>
    <div class="ellipse ellipse-3"></div>
    <div class="head-info">
        <div class="avatar">
            <img src="{{ $card->avatar }}" alt="{{ $card->name }}" class="img-responsive">
        </div>
        @if ( isset($months) )
            <div class="birth-date">
                {{ date('d F', strtotime($card->birth_date)) }}
            </div>
        @endif
        <h2 class="name">
            @include('templates.copyable', ['content' => $card->name])
        </h2>
        <h3 class="position"> {{ $card->position }} </h3>
    </div>
    <div class="body-info">
        <div class="location">
            {{ $card->region }}
        </div>
        <div class="phone">
            @include('templates.copyable', ['content' => $card->phone])
        </div>
        <div class="skype">
            @include('templates.copyable', ['content' => $card->skype])
        </div>
        <div class="email">
            @include('templates.copyable', ['content' => $card->email])
        </div>
        <div class="description">
            {{ $card->description }}
        </div>
    </div>
</div>