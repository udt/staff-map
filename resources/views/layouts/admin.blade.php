<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Staff') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="admin">
    <div id="app">
        @include('layouts.partials.header')

        <section id="secondary-header">
            @include('layouts.partials.sec-header-admin')
        </section>

        <section id="content">
            @yield('content')
        </section>
    </div>

    <!-- Scripts -->
    <script>
        var deleteButtons = document.getElementsByClassName('adm-delete-button');

        [].forEach.call(deleteButtons, function(item) {
            item.onclick = function(e) {
                if (!confirm('Are you sure?')) {
                    e.preventDefault();
                }
            }
        })
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>