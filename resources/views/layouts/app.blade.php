<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Staff') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Font Awesome -->
    <script src="https://use.fontawesome.com/1b4d0cca73.js"></script>
</head>
<body class="@if (isset($page)) {{ $page }} @else home @endif">
    <div id="app">
        @include('layouts.partials.header')

        <section id="secondary-header">
            @yield('secondary-header')
        </section>

        <section id="content">
            @yield('content')
        </section>

        @include('layouts.partials.footer')
    </div>

    <!-- Scripts -->
    <script>
        var copyBtn = document.getElementsByClassName('copy-button');

        [].forEach.call(copyBtn, function(item, i) {

            item.addEventListener('click', function(e) {
                e.preventDefault();

                var text = e.target.nextSibling.nextSibling;

                var range = document.createRange();
                range.selectNode(text);
                window.getSelection().addRange(range);

                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'successful' : 'unsuccessful';
                    console.log('Copy email command was ' + msg);
                } catch(err) {
                    console.log('Oops, unable to copy');
                }
                window.getSelection().removeRange(range);
            });
        });
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
