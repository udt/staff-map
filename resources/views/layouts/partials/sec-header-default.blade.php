<div class="container">
    <div class="nav sec-header-default">
        <div class="filters">
            <div class="text">
                Filter:
            </div>
            <div class="dropdown">
                <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">By Name
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('home.named', ['order' => 'asc']) }}">ASC</a></li>
                    <li><a href="{{ route('home.named', ['order' => 'desc']) }}">DESC</a></li>
                </ul>
            </div>
            <div class="dropdown">
                <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">Department
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="{{route('home', ['department' => 'all'])}}">All</a></li>
                    @foreach($sec_header_departments as $department)
                        <li><a href="{{ route('home', ['department' => $department->id]) }}">{{ $department->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="anchors">
            <a href="https://drive.google.com/drive/u/1/folders/0B0fDME-nZAE6ZGxteERqcC1lZWM" target="_blank"><i class="fa fa-users" aria-hidden="true"></i> Job description</a>
            <a href="https://drive.google.com/drive/u/1/folders/0B0fDME-nZAE6emZFcHQzdkZvTWM" target="_blank"><i class="fa fa-cogs" aria-hidden="true"></i> Instructions for setting up and working with servers</a>
        </div>
    </div>
</div>
