<div class="container">
    <ul class="nav nav-pills sec-header-admin">
        <li class="@if(\Request::route()->getName() == 'admin' ) active @endif">
            <a href="{{ route('admin') }}"> Staff Cards </a>
        </li>
        <li class="@if(\Request::route()->getName() == 'admin.departments' ) active @endif">
            <a href="{{ route('admin.departments') }}"> Departments </a>
        </li>
    </ul>
</div>