<div class="container">
    <ul class="nav nav-pills sec-header-calendar">
        @foreach($months as $index => $month)
            <li class="calendar-menu-item @if( date('m', strtotime( \Carbon\Carbon::now() )) == $index+1) active @endif">
                <a href="#">{{ $month }}</a>
            </li>
        @endforeach
    </ul>

    <script>
        var monthList = document.getElementsByClassName('calendar-menu-item');

        [].forEach.call(monthList, function(item, i) {
            monthList[i].onclick = function() {
                monthList[i].classList.add('active');

                for (var j=0; j<monthList.length; j++) {
                    if (j!==i) {
                        monthList[j].classList.remove('active');
                    }
                }
            }
        })
    </script>
</div>