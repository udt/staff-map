<footer>
    <div class="pre-footer">
        <div class="container">
            <div class="open-office">
                <h2>Responsible for opening an office</h2>

                <div class="row">
                    @foreach($who_can_open as $person)
                        <div class="col-md-3">
                            <div class="item">
                                <div class="image">
                                    <img src="{{ $person->avatar }}" alt="{{ $person->name }}" class="img-responsive">
                                </div>
                                <div class="description">
                                    <h2>{{ $person->name }}</h2>
                                    <h3>{{ $person->position }}</h3>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="close-office">
                <h2>Responsible for closing the office</h2>

                <div class="row">
                    @foreach($who_can_close as $person)
                        <div class="col-md-3">
                            <div class="item">
                                <div class="image">
                                    <img src="{{ $person->avatar }}" alt="{{ $person->name }}" class="img-responsive">
                                </div>
                                <div class="description">
                                    <h2>{{ $person->name }}</h2>
                                    <h3>{{ $person->position }}</h3>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <p>Adress: 69063 Zaporizhzhya St. Nicholas Street 31</p>
                    <p>Email (for general issues) <a href="mailto:info@udtech.co">info@udtech.co</a></p>
                </div>
            </div>
            <div class="ellipse"></div>
            <div class="ellipse ellipse-2"></div>
        </div>
        <div class="mask"></div>
    </div>
</footer>