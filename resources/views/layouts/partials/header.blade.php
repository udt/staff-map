<header>
    <nav class="navbar navbar-static-top">
        <div class="mask"></div>
        <div class="container">
            <div class="ellipse-container">
                <div class="ellipse"></div>
                <div class="ellipse ellipse-2"></div>
            </div>
            <div class="navbar-header"><!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('img/icons/udtech-logo.png') }}" alt="udtech" height="40">
                </a>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('home') }}"
                               class="{{ \Request::route()->getName() === 'home' || \Request::route()->getName() === 'home.named'  ? 'active' : ''}}">
                                Staff</a></li>
                        <li><a href="{{ route('calendar') }}" class="{{ \Request::route()->getName() === 'calendar' ? 'active' : ''}}">Birthdays</a></li>
                        @auth
                            <li><a href="{{ route('admin') }}"  class="{{ \Request::route()->getPrefix() === '/admin' ? 'active' : ''}}">Admin panel</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endauth
                    </ul>
                </div>

                <!-- Search -->
                <div class="navbar-search" id="react-navbar-search"}>
                    <input type="text" placeholder="Loading..." value="Loading..." readonly>
                </div>

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </div>
    </nav>
</header>