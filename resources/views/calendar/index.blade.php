@extends('layouts.app')

@section('secondary-header')
    @include('layouts.partials.sec-header-calendar')
@endsection

@section('content')

    <div class="container" id="calendar-container">
        @for( $i=0;$i<12;$i++ )
            <div class="calendar-month @if( date('m', strtotime( \Carbon\Carbon::now() )) == $i+1) show @endif" id="calendar-month-{{ $i+1 }}">
                <?php $counter = 0; ?>

                <div class="row">

                    @foreach($cards as $card)
                        @if ( date('m', strtotime($card->birth_date)) == $i+1)
                            <div class="col-md-3">
                                @include('templates.card')
                            </div>

                            <?php $counter++; ?>
                        @endif
                    @endforeach
                </div>

                @if ( $counter === 0 )
                    <div class="no-bday">
                        <img src="{{ asset('img/icons/gift-large.png') }}" alt="gift-box" class="gift-large">

                        <p>В этом месяце ни у одного из сотрудников нет Дня Рождения.
                    </div>
                @endif
            </div>
        @endfor

            <script>
                document.getElementsByClassName('sec-header-calendar')[0].onclick = function(){
                    var monthList = this.children;

                    for (var i=0; i<monthList.length; i++) {
                        var month = monthList[i];

                        if (month.classList['value'].indexOf('active')>0) {
                            document.getElementsByClassName("calendar-month")[i].classList.add('show');
                        } else {
                            document.getElementsByClassName("calendar-month")[i].classList.remove('show');
                        }
                    }
                }
            </script>
    </div>

@endsection