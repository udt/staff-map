@extends('layouts.admin')

@section('content')

    <div class="container">
        @if (isset($department))
            <h1>Update Department</h1>
            {{ Form::open(['action' => ['AdminController@updateDepartment', $department->id, 'class' => 'form-group']]) }}
        @else
            <h1>Create Department</h1>
            {{ Form::open(['action' => 'AdminController@storeDepartment', 'class' => 'form-group']) }}
        @endif
            {{Form::token()}}

            <div class="form-group name">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', $value = isset($department) && $department->name ? $department->name : null , ['class' => 'form-control']) }}
            </div>

            <div class="form-group published">
                {{ Form::checkbox('published', 'value', isset($department) ? $department->published : false, ['class' => 'form-check-label']) }}
                {{ Form::label('published', 'Published') }}
            </div>

            <div class="button">
                {{ Form::submit('Save', $attributes = ['class' => 'btn btn-primary']) }}
            </div>

        {{ Form::close() }}

    </div>

@endsection