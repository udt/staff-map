@extends('layouts.admin')

@section('content')

    <div class="container">
        @if (isset($co_worker))
            <h1>Update Card</h1>
            {{ Form::open([
                'action' => ['AdminController@updateCard', $co_worker->id],
                'files' => true,
                'class' => 'form-group'
            ]) }}
        @else
            <h1>Create Card</h1>
            {{ Form::open([
                'action' => 'AdminController@storeCard',
                'files' => true,
                'class' => 'form-group'
            ]) }}
        @endif
            {{Form::token()}}

            <div class="avatar form-group">
                {{ Form::label('avatar', 'Avatar') }}
                @if (isset($co_worker))
                <br/>
                <img
                        style="margin-bottom: 15px"
                        src="{{ asset($co_worker->avatar) }}"
                        alt="{{ str_replace(' ', '-', strtolower( $co_worker->name )) }}-avatar" height="50" />
                @endif
                {{ Form::file('avatar', $attributes = []) }}
            </div>

            <div class="name form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', $value = isset($co_worker) && $co_worker->name ? $co_worker->name : null , ['class' => 'form-control']) }}
            </div>

            <div class="form-group birth_date">
                {{ Form::label('birth_date', 'Birth Date') }}
                {{ Form::date('birth_date', $value = isset($co_worker) && $co_worker->birth_date ? $co_worker->birth_date : \Carbon\Carbon::now(), ['class' => 'form-control']) }}
            </div>

            <div class="form-group position">
                {{ Form::label('position', 'Position') }}
                {{ Form::text('position', $value = $value = isset($co_worker) && $co_worker->position ? $co_worker->position : null, ['class' => 'form-control']) }}
            </div>

            <div class="form-group department">
                {{ Form::label('department', 'Department') }}
                {{ Form::select('department', $departaments, isset($co_worker) && $co_worker->department ? $co_worker->department : null, ['class' => 'form-control']) }}
            </div>

            <div class="form-group region">
                {{ Form::label('region', 'Region') }}
                {{ Form::text('region', $value = isset($co_worker) && $co_worker->region ? $co_worker->region : null, ['class' => 'form-control']) }}
            </div>

            <div class="form-group phone">
                {{ Form::label('phone', 'Phone') }}
                {{ Form::tel('phone', $value = isset($co_worker) && $co_worker->phone ? $co_worker->phone : null, ['class' => 'form-control']) }}
            </div>

            <div class="form-group skype">
                {{ Form::label('skype', 'Skype') }}
                {{ Form::text('skype', $value = isset($co_worker) && $co_worker->skype ? $co_worker->skype : null, ['class' => 'form-control']) }}
            </div>
            <div class="form-group email">
                {{ Form::label('email', 'E-Mail Address') }}
                {{ Form::email('email', $value = isset($co_worker) && $co_worker->email ? $co_worker->email : null, ['class' => 'form-control']) }}
            </div>

            <div class="form-group description">
                {{ Form::label('description', 'Description') }}
                {{ Form::textarea('description', $value = isset($co_worker) && $co_worker->description ? $co_worker->description : null, ['class' => 'form-control']) }}
            </div>

            <div class="form-group open-office">
                {{ Form::checkbox('right_to_open_office', 'value', isset($co_worker) ? $co_worker->right_to_open_office : false, ['class' => 'form-check-label']) }}
                {{ Form::label('right_to_open_office', 'Right to open office') }}
            </div>

            <div class="form-group close-office">
                {{ Form::checkbox('right_to_close_office', 'value', isset($co_worker) ? $co_worker->right_to_close_office : false, ['class' => 'form-check-label']) }}
                {{ Form::label('right_to_close_office', 'Right to close office') }}
            </div>

            <div class="form-group published">
                {{ Form::checkbox('published', 'value', isset($co_worker) ? $co_worker->published : false, ['class' => 'form-check-label']) }}
                {{ Form::label('published', 'Published') }}
            </div>

            <div class="button">
                {{ Form::submit('Save', $attributes = ['class' => 'btn btn-primary']) }}
            </div>

        {{ Form::close() }}
    </div>

@endsection