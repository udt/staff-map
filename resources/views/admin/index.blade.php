@extends('layouts.admin')

@section('content')
    <div class="container">
        <a href="{{ route('admin.card.create') }}" class="btn btn-success">+</a>
        {{ $table->render() }}
    </div>
@endsection