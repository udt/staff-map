@extends('layouts.admin')

@section('content')
    <div class="container">
        <a href="{{ route('admin.department.create') }}" class="btn btn-success">+</a>
        <h5 style="color: red">Note! You can't delete any of departments if there is people in it!</h5>
        {{ $table->render() }}
    </div>
@endsection