@extends('layouts.app')

@section('secondary-header')
    @include('layouts.partials.sec-header-default')
@endsection

@section('content')

    <div class="container">

        @foreach($departments as $dp)
            <section class="department" id="department-{{ $dp->name }}">
                <h3>{{ $dp->name }} department</h3>

                <div class="row">
                    @foreach($cards as $card)
                        @if($card->department === $dp->id)
                        <div class="col-md-3">
                            @include('templates.card')
                        </div>
                        @endif
                    @endforeach
                </div>
            </section>
        @endforeach

    </div>

@endsection