@extends('layouts.app')

@section('secondary-header')
    @include('layouts.partials.sec-header-default')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @foreach($cards as $card)
                <div class="col-md-3">
                    @include('templates.card')
                </div>
            @endforeach
        </div>
    </div>

@endsection