<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Department;
use App\Models\CoWorkerCard;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersSeeder');
        $this->call('DepartmentSeeder');
        $this->call('CoWorkerCardsSeeder');
    }
}

class UsersSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'name' => 'admin',
            'email' => 'udtech.seo@gmail.com',
            'password' => bcrypt('secret'),
            'is_admin' => true
        ]);
    }
}

class DepartmentSeeder extends Seeder
{
    public function run()
    {
        DB::table('departments')->delete();

        Department::create([
            'name' => 'Administration',
            'published' => true
        ]);
        Department::create([
            'name' => 'Design',
            'published' => true
        ]);
        Department::create([
            'name' => 'Programming',
            'published' => true
        ]);
        Department::create([
            'name' => 'Engineering',
            'published' => true
        ]);
        Department::create([
            'name' => 'Financial',
            'published' => true
        ]);
        Department::create([
            'name' => 'HR',
            'published' => true
        ]);
        Department::create([
            'name' => 'PM',
            'published' => true
        ]);
        Department::create([
            'name' => 'Sales',
            'published' => true
        ]);
    }
}

class CoWorkerCardsSeeder extends Seeder
{
    public function run()
    {
        DB::table('co_worker_cards')->delete();

        CoWorkerCard::create([
            "name" => "Igor Kosanovsky",
            "birth_date" => "1982-11-04",
            "position" => "CEO",
            "department" => 1,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "067-612-99-88",
            "skype" => "kigorodes",
            "email" => "igor@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Alexander Kononov",
            "birth_date" => "1972-03-01",
            "position" => "Motion design",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "067-890-45-52",
            "skype" => "cannon_alex",
            "email" => "kononov.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Andrey Arutiunov",
            "birth_date" => "1980-09-22",
            "position" => "Motion design",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095-111-97-86",
            "skype" => "andrey_arutiunov",
            "email" => "arutiunov.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Artem Arefin",
            "birth_date" => "1986-06-27",
            "position" => "Head of the design department",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095 782-56-43",
            "skype" => "artarefin",
            "email" => "artem@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Dmitriy Derevyanko",
            "birth_date" => "1991-07-05",
            "position" => "UI designer",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "063-782-18-64",
            "skype" => "drimerdv",
            "email" => "derevyanko.d@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Irina Bolibok",
            "birth_date" => "1991-07-09",
            "position" => "UI designer",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "067-617-79-68, 063-552-46-53",
            "skype" => "irinabolibok1",
            "email" => "irina@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Margarita Kerdikoshvili",
            "birth_date" => "1997-01-21",
            "position" => "UI designer",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095-411-49-69",
            "skype" => "margarita.k21",
            "email" => "kerdikoshvili.m@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Maria Batura",
            "birth_date" => "1990-08-10",
            "position" => "UI designer",
            "department" => 2,
            "region" => "Dnieper",
            "phone" => "066-95-41-831",
            "skype" => "mary_10_1",
            "email" => "batura.m@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Maria Kovtun",
            "birth_date" => "1981-12-31",
            "position" => "Illustrator",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095-320-50-28",
            "skype" => "mari4ka-31",
            "email" => "kovtun.m@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Nikolai Stanyslavchuk",
            "birth_date" => "1989-09-15",
            "position" => "UI designer",
            "department" => 2,
            "region" => "Khmelnitsk",
            "phone" => "096-263-26-76",
            "skype" => "nicko7x",
            "email" => "stanyslavchuk.n@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);

        CoWorkerCard::create([
            "name" => "Sergey Otrishko",
            "birth_date" => "1988-07-07",
            "position" => "Industrial Designer",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "096-951-32-29",
            "skype" => "s--e--r--g--88",
            "email" => "otrishko.s@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Vadym Tarasov",
            "birth_date" => "1994-06-15",
            "position" => "UI designer",
            "department" => 2,
            "region" => "Vinnitsa",
            "phone" => "097-515-56-14",
            "skype" => "crimea_diver",
            "email" => "tarasov.v@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Vitaliy Olenyak",
            "birth_date" => "1982-08-05",
            "position" => "UX/UI designer",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "063-569-29-94",
            "skype" => "vitaliy6175",
            "email" => "vitaliy@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Yurii Hebura",
            "birth_date" => "1990-05-01",
            "position" => "UI designer",
            "department" => 2,
            "region" => "Uzhgorod",
            "phone" => "050-233-53-18",
            "skype" => "web-lp13",
            "email" => "hebura.y@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Daniel Gostischev",
            "birth_date" => "1988-05-10",
            "position" => "UI designer",
            "department" => 2,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "099-272-47-55",
            "skype" => "daniel-gost",
            "email" => "gostischev.d@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Alexander Volga",
            "birth_date" => "1981-08-22",
            "position" => "Back-end developer",
            "department" => 3,
            "region" => "Kharkiv",
            "phone" => "066-294-61-92",
            "skype" => "valexlg",
            "email" => "volga.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Alexey Gulin",
            "birth_date" => "1985-10-25",
            "position" => "QA engineer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "097-07-621-31",
            "skype" => "akleva",
            "email" => "gulin.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Aleksandr Abramenko",
            "birth_date" => "1984-05-15",
            "position" => "QA engineer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "050-55-55-666",
            "skype" => "alex-abramenko",
            "email" => "abramenko.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Alexey Kolyvayko",
            "birth_date" => "1978-01-22",
            "position" => "Back-end developer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "096-509-37-37, 050-568-08-87",
            "skype" => "kolyvayko",
            "email" => "a.kolyvayko@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Andrey Silivanov",
            "birth_date" => "1990-03-06",
            "position" => "Back-end developer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "066-644-22-63",
            "skype" => "live:30c51886b883d81e",
            "email" => "silivanov.s@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);

        CoWorkerCard::create([
            "name" => "Andrey Yarys",
            "birth_date" => "1973-11-02",
            "position" => "Head of the developer department",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "050-767-59-09, 063-951-21-77",
            "skype" => "live:archyfindev",
            "email" => "yarys@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Anton Synenko",
            "birth_date" => "1991-04-28",
            "position" => "Front-end developer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "066-97-127-28",
            "skype" => "anthony.synenko",
            "email" => "synenko.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Artem Polykarpov",
            "birth_date" => "1984-06-07",
            "position" => "Back-end developer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "067-950-67-50",
            "skype" => "a.polikarpov.amgrade",
            "email" => "a.polykarpov@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Dmitriy Shapovalov",
            "birth_date" => "1991-02-06",
            "position" => "Tech lead of iOS",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "050-322-63-63",
            "skype" => "n00d32",
            "email" => "shapovalov.d@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Igor Karyaka",
            "birth_date" => "1995-07-22",
            "position" => "iOS developer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "063-404-79-38",
            "skype" => "mig_tattoo",
            "email" => "karyaka.i@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Miroslav Krainik",
            "birth_date" => "1987-03-28",
            "position" => "Tech lead of front end",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "068-654-00-10",
            "skype" => "miroslav.krainik",
            "email" => "krainik.m@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Julja Ostrun",
            "birth_date" => "1982-10-12",
            "position" => "Back-end developer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "099-760-16-20",
            "skype" => "live:julja.ostrun",
            "email" => "ostrun.j@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Marat Sataev",
            "birth_date" => "1998-07-22",
            "position" => "DevOps",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "096-631-00-30",
            "skype" => "sataev.marat",
            "email" => "sataev.m@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Artem Shevchenko",
            "birth_date" => "1994-11-13",
            "position" => "iOS developer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "097–044-72-31",
            "skype" => "avtobot_08",
            "email" => "shevchenko.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Irina Skoryk",
            "birth_date" => "1994-04-29",
            "position" => "Front-end developer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095-397-74-54",
            "skype" => "ri_naskoryk",
            "email" => "skoryk.i@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);

        CoWorkerCard::create([
            "name" => "Aleksey Zelenskiy",
            "birth_date" => "1989-07-24",
            "position" => "Front-end developer",
            "department" => 3,
            "region" => "Zaporizhzhya",
            "phone" => "066-663-26-85",
            "skype" => "live:terapylia",
            "email" => "zelenskiy.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Michael Nikolaev",
            "birth_date" => "1990-01-14",
            "position" => "Front-end developer",
            "department" => 3,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095-10-64-654",
            "skype" => "michael_nicolaev",
            "email" => "nikolaev.m@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Alexander Inshakov",
            "birth_date" => "1975-11-18",
            "position" => "Head of the engineer department",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "068-908-34-25, 095-007-02-92",
            "skype" => "inshakov.alexandr",
            "email" => "inshakov.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Andrey Khrushchov",
            "birth_date" => "1978-11-01",
            "position" => "Electronic engineer",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095-604-05-82",
            "skype" => "dusics",
            "email" => "khrushchov.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Andrey Slastyonov",
            "birth_date" => "1990-04-13",
            "position" => "Mechanic engineer",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "096-426-11-83",
            "skype" => "slastonov",
            "email" => "slastyonov.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Dmitriy Klokov",
            "birth_date" => "1980-05-18",
            "position" => "Mechanic engineer",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "067-680-52-09",
            "skype" => "dimec_k",
            "email" => "klokov.d@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Nikolai Blazhenkov",
            "birth_date" => "1983-07-10",
            "position" => "Electronic tech lead",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "097-193-92-26",
            "skype" => "ersted83",
            "email" => "blazhenkov.n@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Pavel Shchypanskyy",
            "birth_date" => "1996-09-19",
            "position" => "Electronic engineer",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095-577-47-91 067-599-07-82",
            "skype" => "pschipanskiy",
            "email" => "shchypanskyy.p@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Ruslan Perepelytsa",
            "birth_date" => "1977-09-08",
            "position" => "Production specialist",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "099-163-85-99, 098-177-78-75",
            "skype" => "live:b15ba05cd810695f",
            "email" => "perepelytsa.r@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Sergey Abakumov",
            "birth_date" => "1990-07-15",
            "position" => "Production specialist",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "050-056-35-44",
            "skype" => "habib153624",
            "email" => "abakumov.s@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);

        CoWorkerCard::create([
            "name" => "Sergey Chervakov",
            "birth_date" => "1987-07-13",
            "position" => "Production specialist",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "096-909-41-77",
            "skype" => "inferno_ve_8",
            "email" => "chervakov.s@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Valerij Stychuk",
            "birth_date" => "1975-09-09",
            "position" => "Mechanic engineer",
            "department" => 4,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "099-714-86-95, 067-439-25-39",
            "skype" => "valera_st75",
            "email" => "stychuk.v@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Angelika Avramenko",
            "birth_date" => "1969-08-28",
            "position" => "Accountant",
            "department" => 5,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "098-250-28-25",
            "skype" => "anzhela3718",
            "email" => "аngelika@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Katerina Riabenko",
            "birth_date" => "1986-03-15",
            "position" => "Assistant accountant",
            "department" => 5,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "066-68-77-553, 068-86-27-202",
            "skype" => "sunnyalf1",
            "email" => "riabenko.e@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Natalyia Dyrda",
            "birth_date" => "1977-06-14",
            "position" => "Financial analyst",
            "department" => 5,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "099-272-35-84, 097-781-31-34",
            "skype" => "d1234568288",
            "email" => "dyrda.n@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Anna Korobka",
            "birth_date" => "1984-08-07",
            "position" => "HR manager",
            "department" => 6,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "066-296-37-49",
            "skype" => "ann_romanjuk",
            "email" => "hr@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Arthur Goncharenko",
            "birth_date" => "1991-11-05",
            "position" => "Recruiter",
            "department" => 6,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "063-484-57-00",
            "skype" => "live:arthurgoncharenko511",
            "email" => "recruiter@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Kateryna Basenko",
            "birth_date" => "1995-06-05",
            "position" => "HR manager",
            "department" => 6,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "099-435-85-08",
            "skype" => "ponka95",
            "email" => "basenko.k@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Vlad Chyrva",
            "birth_date" => "1995-06-27",
            "position" => "Office Manager",
            "department" => 6,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "068-094-52-40",
            "skype" => "vladik0695",
            "email" => "chyrva.v@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Kateryna Abramova",
            "birth_date" => "1986-01-05",
            "position" => "Project manager",
            "department" => 7,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "093-326-68-92",
            "skype" => "live:kate.p.abramova",
            "email" => "abramova.k@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);

        CoWorkerCard::create([
            "name" => "Alexandra Tsvetkova",
            "birth_date" => "1991-04-03",
            "position" => "Project manager",
            "department" => 7,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "093-991-30-03",
            "skype" => "tsvetkova9113",
            "email" => "tsvetkova.a@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Marina Soroka",
            "birth_date" => "1984-10-11",
            "position" => "Project manager",
            "department" => 7,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "050-454-75-41",
            "skype" => "vooyko",
            "email" => "marina@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Alexey Chetverikov",
            "birth_date" => "1981-03-13",
            "position" => "Project manager",
            "department" => 7,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "050-730-13-13, 093-836-02-13",
            "skype" => "alex.set",
            "email" => "alex@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Tania Yakuba",
            "birth_date" => "1981-10-08",
            "position" => "Project manager",
            "department" => 7,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "050-851-34-14",
            "skype" => "tanya_kosanovsky",
            "email" => "tania@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Alex Kruglyakov",
            "birth_date" => "1984-05-26",
            "position" => "Sales manager",
            "department" => 8,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095-653-60-62",
            "skype" => "Faramentor",
            "email" => "alex.kruglyakov@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Andrey Malik",
            "birth_date" => "1983-12-05",
            "position" => "Head of the sales department",
            "department" => 8,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "095-776-27-77",
            "skype" => "mallen.bwt",
            "email" => "andrew@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Dmitriy Filippov",
            "birth_date" => "1983-06-06",
            "position" => "Marketing specialist",
            "department" => 8,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "099-773-76-65",
            "skype" => "filippov.dmitriy.vladimirovich",
            "email" => "filippov.d@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Dmitriy Kuchugurniy",
            "birth_date" => "1994-07-27",
            "position" => "Sales manager",
            "department" => 8,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "063-408-04-35, 091-354-82-03",
            "skype" => "dmytrokuchugurniy",
            "email" => "dmytro@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Jack Tigov",
            "birth_date" => "1991-10-07",
            "position" => "Sales manager",
            "department" => 8,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "066-467-69-66",
            "skype" => "medved4479",
            "email" => "jack@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
        CoWorkerCard::create([
            "name" => "Kirill Gorokhov",
            "birth_date" => "1989-04-04",
            "position" => "Sales manager",
            "department" => 8,
            "region" => "Head office, Zaporizhzhya",
            "phone" => "097-369-48-52, 093-00-55-444",
            "skype" => "snakesfw",
            "email" => "kirill@udtech.co",
            "description" => "",
            "avatar" => "/img/avatars/default.jpg",
            "published" => true
        ]);
    }
}