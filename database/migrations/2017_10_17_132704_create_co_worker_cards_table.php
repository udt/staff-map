<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoWorkerCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('co_worker_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->text('avatar');
            $table->string('name');
            $table->date('birth_date');
            $table->string('position');
            $table->integer('department')->unsigned();
            $table->string('region');
            $table->string('phone')->unique();
            $table->string('skype')->unique();
            $table->string('email')->unique();
            $table->text('description');
            $table->boolean('right_to_open_office')->default(false);
            $table->boolean('right_to_close_office')->default(false);
            $table->boolean('published')->default(false);
            $table->timestamps();
            $table->foreign('department')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('co_worker_cards');
    }
}
