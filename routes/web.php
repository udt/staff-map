<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

Route::get('/named', [
    'as' => 'home.named',
    'uses' => 'HomeController@getByName'
]);

Route::get('/solo/{id}', [
    'as' => 'solo',
    'uses' => 'HomeController@soloById'
]);

Route::get('/calendar', [
    'as' => 'calendar',
    'uses' => 'CalendarController@index'
]);

Route::group([
    'prefix' => 'admin',
    'as' => 'admin',
    'middleware' => 'auth'
], function() {
    Route::get('/', [
        'uses' => 'AdminController@index'
    ]);
    Route::get('/card', [
        'as' => '.card.create',
        'uses' => 'AdminController@createCard'
    ]);
    Route::get('/card/{id}', [
        'as' => '.card.edit',
        'uses' => 'AdminController@editCard'
    ]);
    Route::post('/store', [
        'as' => '.card.store',
        'uses' => 'AdminController@storeCard'
    ]);
    Route::post('/update/{id}', [
        'as' => '.card.update',
        'uses' => 'AdminController@updateCard'
    ]);
    Route::get('/delete/{id}', [
        'as' => '.card.delete',
        'uses' => 'AdminController@destroyCard'
    ]);
    Route::get('/departments', [
        'as' => '.departments',
        'uses' => 'AdminController@departments'
    ]);
    Route::get('/department', [
        'as' => '.department.create',
        'uses' => 'AdminController@createDepartment'
    ]);
    Route::get('/department/{id}', [
        'as' => '.department.edit',
        'uses' => 'AdminController@editDepartment'
    ]);
    Route::post('/department/store', [
        'as' => '.department.store',
        'uses' => 'AdminController@storeDepartment'
    ]);
    Route::post('/department/update/{id}', [
        'as' => '.department.update',
        'uses' => 'AdminController@updateDepartment'
    ]);
    Route::get('/department/delete/{id}', [
        'as' => '.department.delete',
        'uses' => 'AdminController@destroyDepartment'
    ]);
});

Auth::routes();

Route::get('/register', function() {
    return redirect('/');
});