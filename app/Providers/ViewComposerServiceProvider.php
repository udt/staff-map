<?php
declare(strict_types = 1);

namespace App\Providers;

use App\ViewComposer\FooterComposer;
use App\ViewComposer\SecondaryHeaderDefaultComposer;
use App\ViewComposer\SecondaryHeaderCalendarComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

/**
 * Class ViewComposerServiceProvider
 * @package App\Providers
 */
class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        View::composer('layouts.partials.footer', FooterComposer::class);
        View::composer('layouts.partials.sec-header-default', SecondaryHeaderDefaultComposer::class);
        View::composer('layouts.partials.sec-header-calendar', SecondaryHeaderCalendarComposer::class);
    }
}