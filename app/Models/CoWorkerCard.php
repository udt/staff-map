<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoWorkerCard extends Model
{
    /**
     * @var string
     */
    protected $table = 'co_worker_cards';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'avatar',
        'name',
        'birth_date',
        'position',
        'department',
        'region',
        'phone',
        'skype',
        'email',
        'description',
        'right_to_open_office',
        'right_to_close_office',
        'published'

    ];
    public function scopePublished($query) {
        return $query->where('published', true);
    }

    public function scopeWithOpenRights($query) {
        return $query->where('right_to_open_office', true);
    }

    public function scopeWithCloseRights($query) {
        return $query->where('right_to_close_office', true);
    }
}
