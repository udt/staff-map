<?php
declare(strict_types=1);

namespace App\ViewComposer;

use Illuminate\Contracts\View\View,
    App\Models\Department;

/**
 * Class SecondaryHeaderCalendarComposer
 * @package App\ViewComposer
 */
class SecondaryHeaderCalendarComposer
{
    /**
     * FooterComposer constructor.
     * @param FooterComposer $open_rights, $close_rights
     */
    public function __construct()
    {
        //
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $months = [];

        for ($m=1; $m<=12; $m++) {
            $month = date('F', mktime(0,0,0, $m, 1, intval(date('Y'))));
            array_push($months, $month);
        }

        $view->with([
            'months' => $months
        ]);
    }
}