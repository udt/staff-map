<?php
declare(strict_types=1);

namespace App\ViewComposer;

use Illuminate\Contracts\View\View,
    App\Models\Department;

/**
 * Class SecondaryHeaderDefaultComposer
 * @package App\ViewComposer
 */
class SecondaryHeaderDefaultComposer
{
    /**
     * @var
     */
    protected $sec_header_departments;

    /**
     * FooterComposer constructor.
     * @param FooterComposer $open_rights, $close_rights
     */
    public function __construct()
    {
        $this->sec_header_departments = Department::published()->get();
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'sec_header_departments' => $this->sec_header_departments
        ]);
    }
}