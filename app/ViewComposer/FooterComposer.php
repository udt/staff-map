<?php
declare(strict_types=1);

namespace App\ViewComposer;

use App\Models\CoWorkerCard,
    Illuminate\Contracts\View\View;

/**
 * Class FooterComposer
 * @package App\ViewComposer
 */
class FooterComposer
{
    /**
     * @var
     */
    protected $who_can_open;
    protected $who_can_close;

    /**
     * FooterComposer constructor.
     * @param FooterComposer $open_rights, $close_rights
     */
    public function __construct()
    {
        $this->who_can_open = CoWorkerCard::withOpenRights()->get();
        $this->who_can_close = CoWorkerCard::withCloseRights()->get();
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'who_can_open' => $this->who_can_open,
            'who_can_close' => $this->who_can_close
        ]);
    }
}