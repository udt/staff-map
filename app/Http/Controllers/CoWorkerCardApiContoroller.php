<?php

namespace App\Http\Controllers;

use App\Models\CoWorkerCard;
use Illuminate\Http\Request;

class CoWorkerCardApiContoroller extends Controller
{
    public function index()
    {
        return response()->json(
            CoWorkerCard::all(),
            200
        );
    }

    public function serchByName($expression)
    {
        return response()->json(
            CoWorkerCard::where('name', $expression)->orWhere('name', 'like', '%' . $expression . '%')->get(),
            200
        );
    }
}
