<?php

namespace App\Http\Controllers;

use App\Models\CoWorkerCard;
use App\Models\Department;
use Faker\Provider\Image;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = CoWorkerCard::all();

        $table = tableView($cards)
            ->column('Id', 'id')
            ->column('Avatar', 'avatar', 'image|50,50')
            ->column('Name', 'name')
            ->column('Birthday', function ($model) {
                return date('d F Y', strtotime($model->birth_date));
            })
            ->column('Position', 'position')
            ->column('Department', function ($model) {
                return Department::findOrFail($model->department)->name;
            })
            ->column('Region', 'region')
            ->column('Phone', 'phone')
            ->column('Skype', 'skype')
            ->column('Email', 'email')
//                ->column('Description', 'description')
            ->column('Right to open office', 'right_to_open_office', 'boolean|No,Yes')
            ->column('Right to close office', 'right_to_close_office', 'boolean|No,Yes')
            ->column('Published', 'published', 'boolean|No,Yes')
            ->column('', function ($model) {
                return '<a href="' . route('admin.card.edit', ['id' => $model->id]) . '">Edit</a>';
            })
            ->column('', function ($model) {
                return '<a href="' . route('admin.card.delete', ['id' => $model->id]) . '" class="adm-delete-button">Delete</a>';
            })
            ->setTableClass('table table-striped')
            ->useDataTable()
            ->paginate(10);

        return view('admin.index', [
            'table' => $table
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCard() {
        $departments = Department::all();
        $dp_array = [];
        foreach ($departments as $department) {
            $dp_array[$department->id] = $department->name;
        }
        return view('admin.card-form', [
            'departaments' => $dp_array
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function storeCard(Request $request): RedirectResponse
    {
        $avatar = false;

        if ($request->hasFile('avatar')):
            $file = $request->file('avatar');
            $filename = time() . '-' . $file->getClientOriginalName();
            $file->move('img/avatars', $filename);
            $avatar = 'img/avatars/' . $filename;
        endif;

        CoWorkerCard::create([
            'avatar' => $avatar ? $avatar : '/img/avatars/default.jpg',
            'name' => $request->name ? $request->name : '',
            'birth_date' => $request->date ? $request->date : \Carbon\Carbon::now(),
            'position' => $request->position ? $request->position : '',
            'department' => $request->department ? $request->department : '',
            'region' => $request->region ? $request->region : '',
            'phone' => $request->phone ? $request->phone : '',
            'skype' => $request->skype ? $request->skype : '',
            'email' => $request->email ? $request->email : '',
            'description' => $request->description ? $request->description : '',
            'right_to_open_office' => $request->right_to_open_office ? true : false,
            'right_to_close_office' => $request->right_to_close_office ? true : false,
            'published' => $request->published ? true : false
        ]);

        return redirect()->route('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editCard($id) {
        $departments = Department::all();

        $dp_array = [];
        foreach ($departments as $department) {
            $dp_array[$department->id] = $department->name;
        }

        $co_worker = CoWorkerCard::findOrFail($id);

        return view('admin.card-form', [
            'departaments' => $dp_array,
            'co_worker' => $co_worker
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function updateCard(Request $request, $id): RedirectResponse
    {
        $avatar = false;

        if ($request->hasFile('avatar')):
            $file = $request->file('avatar');
            $filename = time() . '-' . $file->getClientOriginalName();
            $file->move('img/avatars', $filename);
            $avatar = 'img/avatars/' . $filename;
        endif;

        $info = [
            'name' => $request->name ? $request->name : '',
            'birth_date' => $request->birth_date ? $request->birth_date : \Carbon\Carbon::now(),
            'position' => $request->position ? $request->position : '',
            'department' => $request->department ? $request->department : '',
            'region' => $request->region ? $request->region : '',
            'phone' => $request->phone ? $request->phone : '',
            'skype' => $request->skype ? $request->skype : '',
            'email' => $request->email ? $request->email : '',
            'description' => $request->description ? $request->description : '',
            'right_to_open_office' => $request->right_to_open_office ? true : false,
            'right_to_close_office' => $request->right_to_close_office ? true : false,
            'published' => $request->published ? true : false
        ];

        if ($avatar) {
            $info['avatar'] = $avatar;
        } else if (!$avatar && !CoWorkerCard::find($id)->value('avatar')) {
            $info['avatar'] = '/img/avatars/default.jpg';
        }

        CoWorkerCard::find($id)->update($info);

        return redirect()->route('admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroyCard($id): RedirectResponse
    {
        CoWorkerCard::destroy($id);

        return redirect()->route('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function departments() {
        $dps = Department::all();

        $table = tableView($dps)
            ->column('Id', 'id')
            ->column('Name', 'name')
            ->column('Published', 'published', 'boolean|No,Yes')
            ->column('', function ($model) {
                return '<a href="' . route('admin.department.edit', ['id' => $model->id]) . '">Edit</a>';
            })
            ->column('', function ($model) {
                return '<a href="' . route('admin.department.delete', ['id' => $model->id]) . '" class="adm-delete-button">Delete</a>';
            })
            ->setTableClass('table table-striped')
            ->useDataTable();

        return view('admin.departments', [
            'table' => $table
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createDepartment() {
        return view('admin.department-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function storeDepartment(Request $request): RedirectResponse
    {
        Department::create([
            'name' => $request->name ? $request->name : '',
            'published' => $request->published ? true : false
        ]);

        return redirect()->route('admin.departments');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editDepartment($id) {
        $department = Department::findOrFail($id);

        return view('admin.department-form', [
            'department' => $department
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function updateDepartment(Request $request, $id): RedirectResponse
    {
        Department::find($id)->update([
            'name' => $request->name ? $request->name : '',
            'published' => $request->published ? true : false
        ]);

        return redirect()->route('admin.departments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroyDepartment($id): RedirectResponse
    {
        Department::destroy($id);

        return redirect()->route('admin.departments');
    }
}
