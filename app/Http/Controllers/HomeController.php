<?php

namespace App\Http\Controllers;

use App\Models\CoWorkerCard;
use App\Models\Department;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public $page = 'home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $order_request = $request->input('order');
//        $order = isset($order_request) && $order_request === 'asc' || isset($order_request) && $order_request === 'desc'
//            ? $order_request : 'asc';

        $department_request = $request->input('department');
        $department = isset($department_request) ? $department_request : false;

        !$department || $department === 'all' ?
            $departments = Department::published()->orderBy('name', 'asc')->get()
        :
            $departments = [Department::findOrFail($department)];

        $cards = CoWorkerCard::published()->orderBy('name', 'asc')->get();

        return view('home.index', [
            'page' => $this->page,
            'departments' => $departments,
            'cards' => $cards
        ]);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getByName(Request $request)
    {
        $order_request = $request->input('order');
        $order = isset($order_request) && $order_request === 'asc' || isset($order_request) && $order_request === 'desc'
            ? $order_request : 'asc';

        $cards = CoWorkerCard::published()->orderBy('name', $order)->get();

        return view('home.named', [
            'page' => $this->page,
            'cards' => $cards
        ]);
    }

    public function soloById($id)
    {
        if ($id) {
            $cards = CoWorkerCard::published()->where('id', $id)->get();

            return $cards
                ? view('home.named', [
                    'page' => $this->page,
                    'cards' => $cards
                ])
                : $this->index();
        }
    }
}
