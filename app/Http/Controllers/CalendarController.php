<?php

namespace App\Http\Controllers;

use App\Models\CoWorkerCard;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public $page = 'calendar';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = CoWorkerCard::published()->orderByRaw("DAY(birth_date)")->get();

        $months = [];

        for ($m=1; $m<=12; $m++) {
            $month = date('F', mktime(0,0,0, $m, 1, intval(date('Y'))));
            array_push($months, $month);
        }

        return view('calendar.index', [
            'page' => $this->page,
            'cards' => $cards,
            'months' => $months
        ]);
    }
}
